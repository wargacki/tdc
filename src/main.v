`timescale 100ps / 1ps

module main
(
    input wire sysclk,
    input wire rst,
    input wire btn,
    input wire [0:0] jc,
    output wire [0:0] ja,
    output wire tx
);

parameter WIDTH = 10;
parameter LENGTH = 6;

wire pulse_out;    
wire [WIDTH*LENGTH-1:0] vec;

wire clk200MHz;

wire uart_write;
wire [7:0] w_data;

clk_wiz_0 u_clk_wiz_0
(
    .reset(0),
    .clk_in1(sysclk),
    .clk_out1(clk200MHz)
);

clocked_delay_line_limited #(.X_START_POS(37), .Y_START_POS(26), .LENGTH(WIDTH*LENGTH)) u_delay_line(
    .clk(clk200MHz),
    .pulse_in(pulse_out),
    .reg_out(vec)
 );
 
(* dont_touch = "true" *) wire [$clog2(WIDTH*LENGTH)*3-1:0] counts;
(* dont_touch = "true" *) reg [$clog2(WIDTH*LENGTH)*3-1:0] counts_reg;

fsm_verilog main_fsm
(
    .clk(clk200MHz), 
    .rst(0),
    .btn(btn),
    .pulse_in(jc[0]),
    .pulse_out(pulse_out)
);

decoder
#(
    .LENGTH(LENGTH),
    .WIDTH(WIDTH),
    .OUTPUTS(3)
) u_decoder
(
    .clk(clk200MHz),
    .rst(rst),
    .vec(vec),
    .counts_out(counts),
    .offsets_out() 
);

uart_ctl u_uart_ctl
(
    .clk(clk200MHz),
    .rst(rst),
    .counts(counts_reg),
    .uart_write(uart_write),
    .w_data(w_data)
);

uart u_uart
(
    .clk(clk200MHz),
    .reset(rst),
    .wr_uart(uart_write),
    .w_data(w_data),
    .tx_full(),
    .tx(tx)
);

always @(posedge clk200MHz) begin
    counts_reg <= counts;
end

(* dont_touch = "true" *) assign ja = &counts_reg;

endmodule

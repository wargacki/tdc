----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2020 21:02:02
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;       -- for the signed, unsigned types and arithmetic ops

entity fsm is
port(
    clk, rst : in std_logic;
    btn : in std_logic;
    pulse_in : in std_logic;
    pulse_out: out std_logic
);
end fsm;

architecture arch of fsm is 
    type state_type is (callib_0, callib_1, callib_2, run);
    signal state_reg, state_nxt : state_type;
    
begin   
    process(clk, rst)
    begin
        if rising_edge(clk) then
            if (rst = '1') then -- go to state zero if reset (how about default?)
                state_reg <= callib_0;
            else
                state_reg <= state_nxt;
            end if; 
        end if;
    end process;

    process(state_reg, btn, pulse_in)
    begin 
        state_nxt <= callib_0; -- (maybe state_reg)???
        case state_reg is 
            when callib_0 =>
                state_nxt <= callib_1; 
                pulse_out <= '0';
            when callib_1 =>
                state_nxt <= callib_2;
                pulse_out <= '1';
            when callib_2 =>
                state_nxt <= run;
                pulse_out <= '0';
            when run =>  
                pulse_out <= pulse_in;
                if btn = '1' then
                    state_nxt <= callib_0;
                else
                    state_nxt <= run;
                end if;
        end case; 
    end process;
end arch; 

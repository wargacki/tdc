`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.01.2020 09:39:43
// Design Name: 
// Module Name: standard
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// The module delays the input data 'din' by the number of clock cycles
// set by CLK_DEL input parameter
module delay
    #( parameter
        WIDTH   = 8, // bit width of the input/output data
        CLK_DEL = 1  // number of clock cycles the data is delayed
    )
    (
        input  wire                   clk, // posedge active clock
        input  wire                   rst, // ASYNC reset active HIGH
        input  wire [ WIDTH - 1 : 0 ] din, // data to be delayed
        output wire [ WIDTH - 1 : 0 ] dout // delayed data
    );

    reg    [ WIDTH - 1 : 0 ] del_mem [ CLK_DEL - 1 : 0 ];

    assign dout = del_mem[ CLK_DEL - 1 ];

//------------------------------------------------------------------------------
// The first delay stage
    always @(posedge clk or posedge rst)
    begin:delay_stage_0
        if(rst)
            del_mem[0] <= 0;
        else
            del_mem[0] <= din;
    end


//------------------------------------------------------------------------------
// All the other delay stages
    genvar                   i;
    generate

        for (i = 1; i < CLK_DEL ; i = i + 1 )
        begin:delay_stage

            always @(posedge clk or posedge rst)
            begin
                if(rst)
                    del_mem[i] <= 0;
                else
                    del_mem[i] <= del_mem[i-1];
            end

        end

    endgenerate

endmodule
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
module multiplexer 
#(parameter
    WIDTH = 1
)
(
    input wire [WIDTH-1:0] inp,
    input wire [WIDTH-1:0] inn,
    input wire ctl,
    output wire [WIDTH-1:0] out
);

    assign out = ctl ? inp : inn;
     
endmodule
///////////////////////////////////////////////
///////////////////////////////////////////////
module multiplexel_many2one
#(parameter
    WIDTH = 2,
    LENGTH = 1
)
(
    input wire [WIDTH*LENGTH-1:0] in,
    input wire [$clog2(WIDTH)-1:0] ctl,
    output wire [LENGTH-1:0] out
);

wire [LENGTH-1:0] mux_out [WIDTH:0];

assign out = mux_out[0];

generate
genvar i;
    for(i = 0; i<WIDTH; i = i+1) begin
        
        multiplexer #(.WIDTH(LENGTH)) mux(
            .inp(in[LENGTH*(i+1)-1:LENGTH*i]),
            .inn(mux_out[i+1]),
            .out(mux_out[i]),
            .ctl(ctl == i)
        );    
    end
    assign mux_out[WIDTH] = in[WIDTH*LENGTH-1: (WIDTH-1)*LENGTH];
endgenerate

endmodule
//////////////////////////////////////////////////////////


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2019 22:42:34
-- Design Name: 
-- Module Name: clocked_delay_line - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library xil_defaultlib;
use xil_defaultlib.tdc_package.all;

entity clocked_delay_line is
    generic (
        X_START_POS :   INTEGER;
        Y_START_POS :   INTEGER;
        LENGTH :        INTEGER);
    port (
        clk :          IN        std_logic;
        pulse_in :     IN        std_logic;
        reg_out :      OUT     std_logic_vector(LENGTH-1 downto 0));
        
end clocked_delay_line;

architecture Behavioral of clocked_delay_line is

	
signal vec : std_logic_vector(LENGTH-1 downto 0);
signal reg : std_logic_vector(LENGTH-1 downto 0);

begin
    u_delay_line : delay_line
        generic map(X_START_POS => X_START_POS, Y_START_POS => Y_START_POS, LENGTH => LENGTH)
        port map(pulse_in => pulse_in, vec_out => vec);  

process (clk) begin
    if rising_edge (clk) then
        reg <= vec;
        reg_out <= reg;        
    end if;
end process;


end Behavioral;

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Pawe� Wargacki
// 
// Create Date: 10.01.2020 23:12:38
// Design Name: 
// Module Name: decoder_2stage
// Project Name: TDC
// Target Devices: 
// Tool Versions: 
// Description: 2nd stage of decoder, with number of outputs as a paramater
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module decoder
#(parameter
    LENGTH = 7,
    WIDTH = 8,
    OUTPUTS = 3
)
(
    input wire clk,
    input wire rst,
    input wire[LENGTH*WIDTH-1:0] vec,
    output wire[OUTPUTS*$clog2(LENGTH*WIDTH)-1:0] counts_out,
    output wire[OUTPUTS*$clog2(LENGTH*WIDTH)-1:0] offsets_out
);

wire [$clog2(LENGTH)*WIDTH-1:0] counts_1stage;
wire [WIDTH-1:0] floe;

decoder_1stage #(.LENGTH(LENGTH), .WIDTH(WIDTH)) u_decoder_1stage
(
    .vec(vec),
    .counts(counts_1stage),
    .floe(floe)
);

wire [$clog2(LENGTH)*WIDTH-1:0] counts_1stage_reg;
wire [WIDTH-1:0] floe_reg;

delay #(.WIDTH($clog2(LENGTH)*WIDTH+WIDTH), .CLK_DEL(1)) u_delay_decoder_1stage
(
    .clk(clk),
    .rst(0),
    .din({counts_1stage, floe}),
    .dout({counts_1stage_reg, floe_reg})
);

//assign floe_reg = floe;
//assign counts_1stage_reg = counts_1stage;

decoder_2stage #(.WIDTH(WIDTH), .OUTPUTS(OUTPUTS), .COUNT_IN_LENGTH($clog2(LENGTH)), .OUTPUTS_LENGTH($clog2(LENGTH*WIDTH))) u_decoder_2stage
(
    .clk(clk),
    .floe(floe_reg),
    .counts_in(counts_1stage_reg),
    .counts_out(counts_out),
    .offsets_out(offsets_out)
);


endmodule
/////////////////////////////////////////////////
/////////////////////////////////////////////////
module decoder_1stage
#(parameter
    LENGTH = 7,
    WIDTH = 8

)
(
    input wire[LENGTH*WIDTH-1:0] vec,
    output wire [$clog2(LENGTH)*WIDTH-1:0] counts,
    output wire [WIDTH-1:0] floe

);

generate 
genvar k;
    for(k = 0; k<WIDTH; k = k+1) begin : _1stages
        decoder_1stage_unit #(.LENGTH(LENGTH)) u_decoder_1stage_unit
        (
            .in(vec[LENGTH*(k+1)-1:LENGTH*k]),
            .count(counts[$clog2(LENGTH)*(k+1)-1:$clog2(LENGTH)*k]),
            .floe(floe[k])
        );
    end
endgenerate

endmodule
////////////////////////////////////////////////
////////////////////////////////////////////////
module decoder_1stage_unit
#(parameter
    LENGTH = 7
)
(
    input wire [LENGTH-1:0] in,
    output reg [$clog2(LENGTH)-1:0] count,
    output reg floe
);

wire [$clog2(LENGTH)-1:0] left_mux_out [0:LENGTH];
wire [$clog2(LENGTH)-1:0] right_mux_out [0:LENGTH];

generate
genvar i, j;

    for(i = 0; i<LENGTH; i = i+1) begin : right_muxes 
        multiplexer #(.WIDTH($clog2(LENGTH))) mux(
            .inp(LENGTH-i),
            .inn(right_mux_out[i+1]),
            .out(right_mux_out[i]),
            .ctl(in == ((2<<(LENGTH-i-1))-1))
        );
    end
    assign right_mux_out[LENGTH] = 0;
    
    for(j = 0; j<LENGTH; j = j+1) begin : left_muxes 
        multiplexer #(.WIDTH($clog2(LENGTH))) mux(
            .inp(j),
            .inn(left_mux_out[j+1]),
            .out(left_mux_out[j]),
            .ctl(in == ((2<<(LENGTH-1))-1 - ((2<<(LENGTH-j-1))-1)))
        );
    end
    assign left_mux_out[LENGTH] = 0;
    
    always@* begin
        
        if(left_mux_out[0] == LENGTH || right_mux_out[0] == LENGTH) begin
            count = LENGTH;
            floe = 0;
        end
        else if(right_mux_out[0] > left_mux_out[0] || (((right_mux_out[0]) == 0) & (left_mux_out[0]) == 0)) begin
            count = right_mux_out[0];
            floe = 1;
        end
        else begin
            count = left_mux_out[0];
            floe = 0;
        end
        
    end

endgenerate

endmodule
//////////////////////////////////////
///////////////////////////////////////
module decoder_2stage
#(parameter
    WIDTH = 8,
    OUTPUTS = 3,
    COUNT_IN_LENGTH = 3,
    OUTPUTS_LENGTH = 6
)
(
    input wire clk,
    input wire[WIDTH-1:0] floe,
    input wire[WIDTH*COUNT_IN_LENGTH-1:0] counts_in,
    
    output reg[OUTPUTS*OUTPUTS_LENGTH-1:0] counts_out,
    output reg[OUTPUTS*OUTPUTS_LENGTH-1:0] offsets_out
);


wire [OUTPUTS_LENGTH-1:0] sum [0:WIDTH-2];
wire [OUTPUTS_LENGTH-1:0] adder_in [0:WIDTH-1];

wire [OUTPUTS*($clog2(WIDTH))-1:0] ctl;

wire [WIDTH-1:0] ctl_reduced;

wire [OUTPUTS-1:0] output_empty;

decoder_2stage_int 
#(
    .WIDTH(WIDTH),
    .OUTPUTS(OUTPUTS),
    .OUTPUTS_LENGTH($clog2(WIDTH))
) u_decoder_2stage_in
(
    .floe(floe),
    .ctl(ctl),
    .output_empty(output_empty)
);


wire[OUTPUTS*$clog2(WIDTH)-1:0] ctl_reg;
wire[OUTPUTS-1:0] output_empty_reg;
wire[WIDTH*COUNT_IN_LENGTH-1:0] counts_in_reg;
wire [WIDTH-1:0] ctl_reduced_reg;


//assign counts_in_reg = counts_in;
//assign output_empty_reg = output_empty;
//assign ctl_reg = ctl;
delay #(.WIDTH(OUTPUTS*$clog2(WIDTH)+OUTPUTS+WIDTH*COUNT_IN_LENGTH+WIDTH), .CLK_DEL(1)) u_delay_decoder_2stage_int
(
    .clk(clk),
    .rst(0),
    .din({ctl, output_empty, counts_in, ctl_reduced}),
    .dout({ctl_reg, output_empty_reg, counts_in_reg, ctl_reduced_reg})
);


assign adder_in[0] = counts_in_reg[COUNT_IN_LENGTH-1:0];

generate
genvar i;
    for(i = 0; i<WIDTH-2; i = i+1) begin : add_stages

        multiplexer #(.WIDTH(OUTPUTS_LENGTH)) mux(
            .inp(0),
            .inn(sum[i]),
            .out(adder_in[i+1]),
            .ctl(ctl_reduced_reg[i])
            );
            
        assign sum[i] = adder_in[i] + counts_in_reg[COUNT_IN_LENGTH*(i+2)-1:COUNT_IN_LENGTH*(i+1)];
                   
        
        decoder_2stage_ctl_reduction #(.WIDTH(OUTPUTS), .LENGTH($clog2(WIDTH))) u_decoder_2stage_ctl_reduction(
            .ctl(ctl),
            .arg(i),
            .ctl_reduced(ctl_reduced[i])
        );
    
    end
endgenerate

assign sum[WIDTH-2] = adder_in[WIDTH-2] + counts_in_reg[COUNT_IN_LENGTH*(WIDTH)-1:COUNT_IN_LENGTH*(WIDTH-1)];


//pack sum outputs;
wire [OUTPUTS_LENGTH*WIDTH-1:0] sum_packed;
generate
genvar k;
for(k =0; k<WIDTH; k= k+1)
    assign sum_packed[OUTPUTS_LENGTH*(k+1)-1:OUTPUTS_LENGTH*k] = sum[k];
endgenerate
//

wire [OUTPUTS_LENGTH*WIDTH-1:0] sum_packed_reg;
wire[OUTPUTS*$clog2(WIDTH)-1:0] ctl_reg_reg;
wire[OUTPUTS-1:0] output_empty_reg_reg;

delay #(.WIDTH(OUTPUTS_LENGTH*WIDTH+OUTPUTS*$clog2(WIDTH)+OUTPUTS), .CLK_DEL(1)) u_delay_decoder_2stage_sum
(
    .clk(clk),
    .rst(0),
    .din({sum_packed, ctl_reg, output_empty_reg}),
    .dout({sum_packed_reg, ctl_reg_reg, output_empty_reg_reg})
);

wire [OUTPUTS_LENGTH-1:0] output_mux [OUTPUTS-1:0];

generate 
genvar j;
    for(j = 0; j<OUTPUTS; j = j+1) begin

        multiplexel_many2one
        #(
            .WIDTH(WIDTH-1),
            .LENGTH(OUTPUTS_LENGTH)
        ) u_multiplexel_many2one
        (
            .in(sum_packed_reg),
            .ctl(ctl_reg_reg[$clog2(WIDTH-1)*(j+1)-1:$clog2(WIDTH-1)*j]),
            .out(output_mux[j])
        );
        
        always @*
            if(output_empty_reg_reg[j])
//                counts_out[OUTPUTS_LENGTH*(OUTPUTS - j)-1:OUTPUTS_LENGTH*(OUTPUTS - j - 1)] = 0;
//            else
//                counts_out[OUTPUTS_LENGTH*(OUTPUTS - j)-1:OUTPUTS_LENGTH*(OUTPUTS - j - 1)] = output_mux[j];
                counts_out[OUTPUTS_LENGTH*(j+1)-1:OUTPUTS_LENGTH*j] = 0;
            else
                counts_out[OUTPUTS_LENGTH*(j+1)-1:OUTPUTS_LENGTH*j] = output_mux[j];
    end
endgenerate

endmodule

///////////////////////////////////////////////////////////
module decoder_2stage_ctl_reduction
#(parameter
    LENGTH = 3,
    WIDTH = 3
)
(
    input wire [WIDTH*LENGTH-1:0] ctl,
    input wire [LENGTH-1:0] arg,
    output wire ctl_reduced
);

wire [WIDTH-1:0] ctl_equals_arg;
generate
genvar i;
    for(i = 0; i < WIDTH; i = i+1) begin
        assign ctl_equals_arg[i] = (ctl[LENGTH*(i+1)-1:LENGTH*i] == arg);
    end
endgenerate
assign ctl_reduced = |ctl_equals_arg;
endmodule
//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
module decoder_2stage_int
#(parameter
    WIDTH = 8,
    OUTPUTS = 3,
    OUTPUTS_LENGTH = 3
)
(
    input wire[WIDTH-1:0] floe,
    output wire[OUTPUTS*OUTPUTS_LENGTH-1:0] ctl,
    output wire[OUTPUTS-1:0] output_empty
);

wire [WIDTH-1:0] mux_ctl [0:OUTPUTS-1];
wire [OUTPUTS_LENGTH-1:0] mux_out [0:(WIDTH+1)*OUTPUTS-1];

generate
genvar i_outs, i_mux;
    for(i_outs=0; i_outs<OUTPUTS; i_outs=i_outs+1) begin : outputs
        for(i_mux=0; i_mux<WIDTH-i_outs-1; i_mux = i_mux+1) begin : muxes                       
            multiplexer #(.WIDTH(OUTPUTS_LENGTH)) mux(
                .inp(i_mux+i_outs),
                .inn(mux_out[i_outs*(WIDTH+1) + i_mux+1]),
                .out(mux_out[i_outs*(WIDTH+1) + i_mux]),
                .ctl(mux_ctl[i_outs][i_mux])
            );
            if(i_outs == 0)
                assign mux_ctl[0][i_mux] = floe[i_mux+1] & !floe[i_mux];
            else
                assign mux_ctl[i_outs][i_mux] = (floe[i_mux+i_outs+1] & !floe[i_mux+i_outs] & (|mux_ctl[i_outs - 1][i_mux:0]));
        end
        assign mux_out[i_outs * (WIDTH+1) + WIDTH-i_outs-1] = WIDTH-1;
        if(i_outs == 0)
            assign output_empty[i_outs] = (mux_out[i_outs * (WIDTH+1)] == WIDTH-1) & (floe[WIDTH-1]);
        else
            assign output_empty[i_outs] = (output_empty[i_outs-1]) | ((mux_out[i_outs * (WIDTH+1)] == WIDTH-1) & (floe[WIDTH-1])) | ((!output_empty[i_outs-1]) & (mux_out[(i_outs-1) * (WIDTH+1)] == WIDTH-1));
        assign ctl[OUTPUTS_LENGTH*(i_outs+1)-1:OUTPUTS_LENGTH*i_outs] = mux_out[i_outs*(WIDTH+1)];       
    end 
endgenerate

endmodule
////////////////////////////////////////////////////////

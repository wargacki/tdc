`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.01.2020 20:41:35
// Design Name: 
// Module Name: tb_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_decoder(


    );
    

reg [6*10-1:0] vec;
reg pulsein;
wire pulseout;
reg [0:0] btn;

wire [5:0] count1;
wire [5:0] count2;
wire [5:0] count3;

reg clk;

always
 begin
    clk = 1'b0;
    #1;
    clk = 1'b1;
    #1;
end

fsm main_fsm(
    .clk(clk), 
    .rst(0),
    .btn(btn),
    .pulse_in(pulsein),
    .pulse_out(pulseout)
);

decoder
#(
    .LENGTH(6),
    .WIDTH(10),
    .OUTPUTS(3)
) u_decoder
(
    .clk(clk),
    .rst(0),
    .vec(vec),
    .counts_out({count3, count2, count1}),
    .offsets_out() 
);  
    
initial begin
    btn <= 1'b0;
    pulsein <= 1'b0;
    vec <= 60'b111111111111111111111111111111111111000000000000000000111111;
    #5
    btn <= 1'b1;
    #5
    btn <= 1'b0;
    #10    
    pulsein <= 1'b1;
    #5
    btn <= 1'b1;
    #5
    btn <= 1'b0;
    #10
    $stop;
    
end 

endmodule

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2019 12:57:16
-- Design Name: 
-- Module Name: delay_line - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: modified module delay_line, only reading co[1] from carry4 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.MATH_REAL.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity delay_line_limited is
    generic (
        X_START_POS :   INTEGER;
        Y_START_POS :   INTEGER;
        LENGTH :        INTEGER);
    port (
        pulse_in :     IN        std_logic;
        vec_out :      INOUT     std_logic_vector(LENGTH-1 downto 0));

end delay_line_limited;

architecture Behavioral of delay_line_limited is

	ATTRIBUTE LOC			 	         : string;
	ATTRIBUTE dont_touch 	             : string;
	ATTRIBUTE dont_touch OF Behavioral	 : ARCHITECTURE IS "true";
	
	signal carry_out : std_logic_vector(LENGTH*4-1 downto 0);

	
BEGIN 
	-- Generation of the carrychain, starting at the specified X, Y coordinate. 
	carry_delay_line: FOR i IN 0 TO LENGTH-1 GENERATE
	
		process (carry_out) begin
			vec_out(i) <= carry_out(4*(i+1)-1);
		end process;
		
		first_carry4: IF i = 0 GENERATE
		
	       ATTRIBUTE LOC OF delayblock : LABEL IS "SLICE_X"&INTEGER'image(X_START_POS)&"Y"&INTEGER'image(Y_START_POS+i);
			
		BEGIN
		
			delayblock: CARRY4 
				PORT MAP(
					CO 		=> carry_out(3 DOWNTO 0),
					CI 		=> pulse_in,
					CYINIT   => '0',
					DI 		=> "0000",
					S 		=> "1111");
         END GENERATE;
		 
         next_carry4: IF i > 0 GENERATE
		 
			ATTRIBUTE LOC OF delayblock : LABEL IS "SLICE_X"&INTEGER'image(X_START_POS)&"Y"&INTEGER'image(Y_START_POS+i);
			
		BEGIN
		
            delayblock: CARRY4 
				PORT MAP(
					CO 		=> carry_out(4*(i+1)-1 DOWNTO 4*i),
					CI 		=> carry_out(4*i-1),
					CYINIT 	=> '0',
					DI 		=> "0000",
					S 		=> "1111");
         END GENERATE;
    END GENERATE;
    

end Behavioral;


module uart_ctl
(
    input wire clk,
    input wire rst,
    input wire [17:0] counts,
    output reg uart_write,
    output reg [7:0] w_data
);


localparam WAIT = 1'b0,
           SEND = 1'b1;

reg state, state_nxt;
reg uart_write_nxt;
reg [7:0] w_data_nxt;
reg [1:0] byte_ctr, byte_ctr_nxt;
reg counts_read, counts_read_nxt;
reg counts_write, counts_write_nxt;
wire empty, full;
reg [17:0] new_counts, new_counts_nxt;
wire [17:0] fifo_counts;

fifo
# (.B(18), .W(6)) big_fifo_tx_unit
(
   .clk(clk), .reset(rst), .rd(counts_read),
   .wr(counts_write), .w_data(new_counts), .empty(empty),
   .full(full), .r_data(fifo_counts)
);

always @(posedge clk)
begin
    if (rst)
    begin
        counts_read <= 0;
        counts_write <= 0;
        new_counts <= 18'b0;
        byte_ctr <= 0;
        state <= WAIT;
        uart_write <= 0;
        w_data <= 8'b0;
    end
    else
    begin
        counts_read <= counts_read_nxt;
        counts_write <= counts_write_nxt;
        new_counts <= new_counts_nxt;
        byte_ctr <= byte_ctr_nxt;
        state <= state_nxt;
        uart_write <= uart_write_nxt;
        w_data <= w_data_nxt;
    end
end

always @*
begin
    counts_read_nxt = 0;
    counts_write_nxt = 0;
    new_counts_nxt = 18'b0;
    state_nxt = WAIT;
    w_data_nxt = 8'b0;
    if (counts != 0 && !full)
    begin
        counts_write_nxt = 1;
        new_counts_nxt = counts;
    end
    case (state)
        WAIT:
        begin
            byte_ctr_nxt = 0;
            uart_write_nxt = 0;
            if (!empty)
            begin
                counts_read_nxt = 1;
                state_nxt = SEND;
            end
        end
        SEND:
        begin
            byte_ctr_nxt = byte_ctr + 1;
            uart_write_nxt = 1;
            case (byte_ctr)
                0: begin w_data_nxt = {2'b00, fifo_counts[17:12]}; end
                1: begin w_data_nxt = {2'b00, fifo_counts[11:6]}; end
                2: begin w_data_nxt = {2'b00, fifo_counts[5:0]}; end
            endcase
            if (byte_ctr != 2)
                state_nxt = SEND;
        end
    endcase
end

endmodule
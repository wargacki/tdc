`timescale 1ns / 1ps

module main_tb;
    
    reg clk;
    wire rst;
    wire btn;
    reg pulsein;
    wire pulseout;
    wire tx;
    
    always
    begin
        clk = 1'b0;
        #5;
        clk = 1'b1;
        #5;
    end    
    
    initial begin
        #500
        $stop;

    end
    
    main main_test
    (
        .sysclk(clk),
        .rst(0),
        .btn(0),
        .jc(0),
        .ja(pulseout),
        .tx(tx)
    );
    
    
endmodule

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2019 23:42:53
-- Design Name: 
-- Module Name: tdc_package - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package tdc_package is

component delay_line is
    generic (
        X_START_POS :   INTEGER;
        Y_START_POS :   INTEGER;
        LENGTH :        INTEGER);
    port (
        pulse_in :     IN        std_logic;
        vec_out :      INOUT     std_logic_vector(LENGTH-1 downto 0));
    
end component delay_line;


component clocked_delay_line is
    generic (
        X_START_POS :   INTEGER;
        Y_START_POS :   INTEGER;
        LENGTH :        INTEGER);
    port (
        clk :          IN        std_logic;
        pulse_in :     IN        std_logic;
        reg_out :      OUT     std_logic_vector(LENGTH-1 downto 0));
    
end component clocked_delay_line;


component delay_line_limited is
    generic (
        X_START_POS :   INTEGER;
        Y_START_POS :   INTEGER;
        LENGTH :        INTEGER);
    port (
        pulse_in :     IN        std_logic;
        vec_out :      INOUT     std_logic_vector(LENGTH-1 downto 0));
    
end component delay_line_limited;


component clocked_delay_line_limited is
    generic (
        X_START_POS :   INTEGER;
        Y_START_POS :   INTEGER;
        LENGTH :        INTEGER);
    port (
        clk :          IN        std_logic;
        pulse_in :     IN        std_logic;
        reg_out :      OUT     std_logic_vector(LENGTH-1 downto 0));
    
end component clocked_delay_line_limited;


end package;

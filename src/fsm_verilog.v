`timescale 1ns / 1ps

module fsm_verilog(
    input wire clk,
    input wire rst,
    input wire btn,
    input wire pulse_in,
    output reg pulse_out
    );
    
    localparam 
        CALLIB0     = 2'b00,
        CALLIB1     = 2'b01,
        CALLIB2     = 2'b10,
        RUN         = 2'b11;

    reg [1:0] state = 2'b00, state_nxt = 2'b00;
    reg pulse_nxt = 1'b0;
    
    always @(posedge clk)
        if(!rst)
            begin
                pulse_out       <= pulse_nxt;
                state           <= state_nxt;
            end
        else
            begin
                pulse_out       <= 1'b0;
                state           <= CALLIB0;
            end
    
    always@(state or pulse_in or btn)
        begin
            case(state)
                CALLIB0:    state_nxt = CALLIB1;
                CALLIB1:    state_nxt = CALLIB2;
                CALLIB2:    state_nxt = RUN;
                RUN:        state_nxt = btn ? CALLIB0 : RUN;
                default:    state_nxt = CALLIB0;
            endcase
        end

    always @(state or pulse_out or pulse_in)
        begin
            pulse_nxt = pulse_out;
            case(state)
                CALLIB0:
                    pulse_nxt = 1'b0;
                CALLIB1:
                    pulse_nxt = 1'b1;
                CALLIB2:
                    pulse_nxt = 1'b0; 
                RUN:
                    pulse_nxt = pulse_in;
            endcase
        end
endmodule
